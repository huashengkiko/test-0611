import cookie from 'js-cookie'

// 最好提前在你的 store 中初始化好所有所需属性
// https://vuex.vuejs.org/zh-cn/mutations.html
export const state = () => ({
  // 这两个用于client side的使用, 又放cookie里是为了刷新时状态不丢失
  userId: '',
  token: '',
  tenantId: '',
  meta: {},

  user: {},
  menuList: [],
  permission: {}
})

//  mutation 必须同步执行
export const mutations = {
  login(state, payload) {
    state.token = payload.key
    state.userId = payload.id
    state.tenantId = payload.tenantId

    // 部署不一定是在根路径, 所以cookie要设置path
    let path = this.$router.options.base
    cookie.set('token', payload.key, {path})
    cookie.set('userId', payload.id, {path})
    cookie.set('tenantId', payload.tenantId, {path})
  },
  logout(state) {
    state.token = ''
    state.userId = ''
    state.tenantId = ''

    let path = this.$router.options.base
    cookie.remove('token', {path})
    cookie.remove('userId', {path})
    cookie.remove('tenantId', {path})
  },
  update(state, payload) {
    Object.keys(payload).forEach(k => {
      state[k] = payload[k]
    })
  }
}

// Action 提交的是 mutation，而不是直接变更状态
// Action 可以包含任意异步操作
export const actions = {
  async login(context, payload) {
    // store 对象
    // console.log(context)
    let {commit, state, dispatch} = context

    let resp = await this.$axios.$post(`/security/api/v1/users/login`, payload)
    commit('login', resp